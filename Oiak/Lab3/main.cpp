#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include <time.h>

//Links to articles -- "https://www.codeproject.com/Articles/15971/Using-Inline-Assembly-in-C-C" and "https://gcc.gnu.org/onlinedocs/gcc/Extended-Asm.html#Extended-Asm" and "https://en.wikibooks.org/wiki/X86_Assembly/SSE" and "http://students.mimuw.edu.pl/~zbyszek/asm/pl/instrukcje-sse.html"
//Debbugging useful commands --> ggdb i r xmm0 | i r sse | disass | x/f memory | x/sizeOfVectorf memory

//2048 <--> (x0+x1+x2+x3)*512; 4096 <--> 4 * 1024; 8192 <-->  4* 2048


#define rozmiar_vectora 512 // sizes -> 512 v 1024 v 2048 v 8192
#define ilosc_pomiarow 10
#define zaokraglenie 10.0f

//time
#define nanosekundy  CLOCKS_PER_SEC * 1000000000.0;
#define mikrosekundy  CLOCKS_PER_SEC * 1000000.0;
#define milisekudny CLOCKS_PER_SEC * 1000.0;


//variables to count avarage.
float dzielnik = ilosc_pomiarow * rozmiar_vectora;

clock_t start,end;
double czas_dodawania = 0, czas_odejmowania = 0, czas_mnożenia = 0, czas_dzielnia = 0;
double sredni_czas_dodawania =0, sredni_czas_odejmowania =0, sredni_czas_mnozenia =0, sredni_czas_dzielenia = 0;



struct vector{
    float x0,x1,x2,x3;
};

struct vector vector_1[rozmiar_vectora];
struct vector vector_2[rozmiar_vectora];


void inicjalizacja_vectora_liczba(float number){
    for(int i=0; i < rozmiar_vectora; i++){
        vector_1[i].x0 = number;
        vector_1[i].x1 = number;
        vector_1[i].x2 = number;
        vector_1[i].x3 = number;
        vector_2[i].x0 = number;
        vector_2[i].x1 = number;
        vector_2[i].x2 = number;
        vector_2[i].x3 = number;
    }
}


void inicjalizacja_vectora_losowymi_liczbami(){
    for(int i=0; i < rozmiar_vectora; i++){
        vector_1[i].x0 = (float)(rand() % 1000000) / zaokraglenie;
        vector_1[i].x1 = (float)(rand() % 1000000) / zaokraglenie;
        vector_1[i].x2 = (float)(rand() % 1000000) / zaokraglenie;
        vector_1[i].x3 = (float)(rand() % 1000000) / zaokraglenie;
        vector_2[i].x0 = (float)(rand() % 1000000) / zaokraglenie;
        vector_2[i].x1 = (float)(rand() % 1000000) / zaokraglenie;
        vector_2[i].x2 = (float)(rand() % 1000000) / zaokraglenie;
        vector_2[i].x3 = (float)(rand() % 1000000) / zaokraglenie;
    }
}


struct vector suma_SIMD[rozmiar_vectora];
struct vector roznica_SIMD[rozmiar_vectora];
struct vector iloczyn_SIMD[rozmiar_vectora];
struct vector iloraz_SIMD[rozmiar_vectora];


void dodawanie_SIMD(struct vector vector_1, struct vector vector_2, struct  vector* resultVector){
    __asm__(
            "movaps %1, %%xmm0;"                      //przenies argument do rejestru %%xmm
            "movaps %2, %%xmm1;"
            "addps %%xmm1, %%xmm0;"                  //dodaj vectory i prrzechowaj wynik w %%xmm0
            "movaps %%xmm0, %0;"
            : "=m" (*resultVector)                  //litera (m- memory, =write-only mode output)     0
            : "m" (vector_1),"m" (vector_2) //first input --> firstVector, second input --> secondVector   1,2
            );
};


void odejmowanie_SIMD(struct vector firstVector, struct vector secondVector, struct  vector* resultVector){
    __asm__(
            "movaps %1, %%xmm0;"
            "movaps %2, %%xmm1;"
            "subps %%xmm1, %%xmm0;"
            "movaps %%xmm0, %0;"
            : "=m" (*resultVector)   //0;
            : "m" (firstVector),"m" (secondVector) //1,2
            );
}


void mnozenie_SIMD(struct vector firstVector, struct vector secondVector, struct  vector* resultVector){
    __asm__(
            "movaps %1, %%xmm0;"
            "movaps %2, %%xmm1;"
            "mulps %%xmm1, %%xmm0;"
            "movaps %%xmm0, %0;"
            : "=m" (*resultVector)
            : "m" (firstVector),"m" (secondVector)
            );
}


void dzielenie_SIMD(struct vector firstVector, struct vector secondVector, struct  vector* resultVector){
    __asm__(
            "movaps %1, %%xmm0;"
            "movaps %2, %%xmm1;"
            "divps %%xmm1, %%xmm0;"
            "movaps %%xmm0, %0;"
            : "=m" (*resultVector)
            : "m" (firstVector),"m" (secondVector)
            );
}


void testAllSIMD(){

    for(int i=0; i < ilosc_pomiarow; i++){

        for(int j=0; j < rozmiar_vectora; j++){
            start = clock();
            dodawanie_SIMD(vector_1[i], vector_2[i], &suma_SIMD[i]);
            end = clock();
            czas_dodawania += ((double) (end - start)) / mikrosekundy ;
        }


        for(int j=0; j < rozmiar_vectora; j++){
            start = clock();
            odejmowanie_SIMD(vector_1[i], vector_2[i], &roznica_SIMD[i]);
            end = clock();
            czas_odejmowania += ((double) (end - start)) / mikrosekundy;
        }


        for(int j=0; j < rozmiar_vectora; j++){
            start = clock();
            mnozenie_SIMD(vector_1[i], vector_2[i], &iloczyn_SIMD[i]);
            end = clock();
            czas_mnożenia += ((double) (end - start)) / mikrosekundy;
        }

        for(int j=0; j < rozmiar_vectora; j++){
            start = clock();
            dzielenie_SIMD(vector_1[i], vector_2[i], &iloraz_SIMD[i]);
            end = clock();
            czas_dzielnia += ((double) (end - start)) / mikrosekundy;
        }
    }

    sredni_czas_dodawania = czas_dodawania / dzielnik;
    printf("+ ");
    printf("%f\n", sredni_czas_dodawania);

    sredni_czas_odejmowania = czas_odejmowania / dzielnik;
    printf("- ");
    printf("%f\n", sredni_czas_odejmowania);

    sredni_czas_mnozenia = czas_mnożenia / dzielnik;
    printf("* ");
    printf("%f\n", sredni_czas_mnozenia);

    sredni_czas_dzielenia = czas_dzielnia / dzielnik;
    printf("/ ");
    printf("%f\n", sredni_czas_dzielenia);
}


struct vector suma_SISD[rozmiar_vectora];
struct vector roznica_SISD[rozmiar_vectora];
struct vector iloczyn_SISD[rozmiar_vectora];
struct vector iloraz_SISD[rozmiar_vectora];


void addSISD(struct vector firstVector, struct vector secondVector,struct  vector* resultVector ){
    __asm__(
        // adding x0
            "fld %4;"
            "fadd %8;"
            "fstp %0;"

            // adding x1
            "fld %5;"
            "fadd %9;"
            "fstp %1;"

            //adding x2
            "fld %6;"
            "fadd %10;"
            "fstp %2;"

            //adding x3
            "fld %7;"
            "fadd %11;"
            "fstp %3;"

            :
            "=m"((*resultVector).x0), //0
            "=m"((*resultVector).x1), //1
            "=m"((*resultVector).x2), //2
            "=m"((*resultVector).x3)  //3

            :
            "m"(firstVector.x0), //4
            "m"(firstVector.x1), //5
            "m"(firstVector.x2), //6
            "m"(firstVector.x3), //7
            "m"(secondVector.x0), //8
            "m"(secondVector.x1), //9
            "m"(secondVector.x2), //10
            "m"(secondVector.x3) //11

            );
}


void subSISD(struct vector firstVector, struct vector secondVector,struct  vector* resultVector ){
    __asm__(
        // adding x0
            "fld %4;"
            "fsub %8;"
            "fstp %0;"

            // adding x1
            "fld %5;"
            "fsub %9;"
            "fstp %1;"

            //adding x2
            "fld %6;"
            "fsub %10;"
            "fstp %2;"

            //adding x3
            "fld %7;"
            "fsub %11;"
            "fstp %3;"

            : "=m"((*resultVector).x0),
    "=m"((*resultVector).x1),
    "=m"((*resultVector).x2),
    "=m"((*resultVector).x3)

            : "m"(firstVector.x0),
    "m"(firstVector.x1),
    "m"(firstVector.x2),
    "m"(firstVector.x3),
    "m"(secondVector.x0),
    "m"(secondVector.x1),
    "m"(secondVector.x2),
    "m"(secondVector.x3)

            );
}


void mulSISD(struct vector firstVector, struct vector secondVector,struct  vector* resultVector ){
    __asm__(
        // adding x0
            "fld %4;"
            "fmul %8;"
            "fstp %0;"

            // adding x1
            "fld %5;"
            "fmul %9;"
            "fstp %1;"

            //adding x2
            "fld %6;"
            "fmul %10;"
            "fstp %2;"

            //adding x3
            "fld %7;"
            "fmul %11;"
            "fstp %3;"

            : "=m"((*resultVector).x0),
    "=m"((*resultVector).x1),
    "=m"((*resultVector).x2),
    "=m"((*resultVector).x3)

            : "m"(firstVector.x0),
    "m"(firstVector.x1),
    "m"(firstVector.x2),
    "m"(firstVector.x3),
    "m"(secondVector.x0),
    "m"(secondVector.x1),
    "m"(secondVector.x2),
    "m"(secondVector.x3)

            );
}


void divSISD(struct vector firstVector, struct vector secondVector,struct  vector* resultVector ){
    __asm__(
        // adding x0
            "fld %4;"
            "fdiv %8;"
            "fstp %0;"

            // adding x1
            "fld %5;"
            "fdiv %9;"
            "fstp %1;"

            //adding x2
            "fld %6;"
            "fdiv %10;"
            "fstp %2;"

            //adding x3
            "fld %7;"
            "fdiv %11;"
            "fstp %3;"

            : "=m"((*resultVector).x0),
    "=m"((*resultVector).x1),
    "=m"((*resultVector).x2),
    "=m"((*resultVector).x3)

            : "m"(firstVector.x0),
    "m"(firstVector.x1),
    "m"(firstVector.x2),
    "m"(firstVector.x3),
    "m"(secondVector.x0),
    "m"(secondVector.x1),
    "m"(secondVector.x2),
    "m"(secondVector.x3)

            );
}


void testAllSISD(){

    for(int i=0; i < ilosc_pomiarow; i++){

        for(int j=0; j < rozmiar_vectora; j++){
            start = clock();
            addSISD(vector_1[i], vector_2[i], &suma_SISD[i]);
            end = clock();
            czas_dodawania += ((double) (end - start)) / mikrosekundy;
        }

        for(int j=0; j < rozmiar_vectora; j++){
            start = clock();
            subSISD(vector_1[i], vector_2[i], &roznica_SISD[i]);
            end = clock();
            czas_odejmowania += ((double) (end - start)) / mikrosekundy;
        }

        for(int j=0; j < rozmiar_vectora; j++){
            start = clock();
            mulSISD(vector_1[i], vector_2[i], &iloczyn_SISD[i]);
            end = clock();
            czas_mnożenia += ((double) (end - start)) / mikrosekundy;
        }

        for(int j=0; j < rozmiar_vectora; j++){
            start = clock();
            divSISD(vector_1[i], vector_2[i], &iloraz_SISD[i]);
            end = clock();
            czas_dzielnia += ((double) (end - start)) / mikrosekundy;
        }
    }

    sredni_czas_dodawania = czas_dodawania / dzielnik;
    printf("+ ");
    printf("%f\n", sredni_czas_dodawania);

    sredni_czas_odejmowania = czas_odejmowania / dzielnik;
    printf("- ");
    printf("%f\n", sredni_czas_odejmowania);

    sredni_czas_mnozenia = czas_mnożenia / dzielnik;
    printf("* ");
    printf("%f\n", sredni_czas_mnozenia);

    sredni_czas_dzielenia = czas_dzielnia / dzielnik;
    printf("/ ");
    printf("%f\n", sredni_czas_dzielenia);
}



int main(){

        srand(time(NULL));
        inicjalizacja_vectora_losowymi_liczbami();
        //inicjalizacja_vectora_liczba(1.0);

        //SIDM
        char type[] = "Typ obliczen: SIMD\n";
        char numbers[] = "Liczba liczb: = ";
        char time[] = "Sredni czas[us]:";
        char naglowek[] = "\nNOWE\n";


        printf("%s", naglowek);
        printf("%s", type);
        printf("%s", numbers);
        printf("%i\n", 4 * rozmiar_vectora);
        printf("%s\n", time);

        testAllSIMD();

        FILE *fPtr;

        fPtr = fopen("dataSIMD.txt", "w");

        if (fPtr == NULL) {
            printf("Nie udalo sie otworzyc pliku. \n");
            exit(EXIT_FAILURE);
        }


        fprintf(fPtr, "%s%s%i\n%s\n+ %f\n- %f\n* %f\n/ %f\n", type, numbers, 4 * rozmiar_vectora, time,
                sredni_czas_dodawania, sredni_czas_odejmowania, sredni_czas_mnozenia, sredni_czas_dzielenia);
        fclose(fPtr);

        //SISD



        char type_two[] = "Typ obliczen: SISD\n";

        printf("%s", type_two);
        printf("%s", numbers);
        printf("%i\n", 4 * rozmiar_vectora);
        printf("%s\n", time);

        testAllSISD();


        fPtr = fopen("dataSISD.txt", "w");

        if (fPtr == NULL) {
            printf("Nie udalo sie otworzyc pliku. \n");
            exit(EXIT_FAILURE);
        }


        fprintf(fPtr, "%s%s%i\n%s\n+ %f\n- %f\n* %f\n/ %f\n", type, numbers, 4 * rozmiar_vectora, time,
                sredni_czas_dodawania, sredni_czas_odejmowania, sredni_czas_mnozenia, sredni_czas_dzielenia);
        fclose(fPtr);
    

    printf("\nGoodbye!\n");

    return 0;
}