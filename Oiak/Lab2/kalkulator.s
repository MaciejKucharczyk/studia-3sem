SYSEXIT = 1;
EXIT_SUCCESS = 0;
SYSREAD = 3
STDIN = 0
STDOUT = 1
SYSWRITE = 4

.data

float1: .float 1.6e1
float2: .float 1.24e3
float3: .float -1.2e1

double1: .double 1.9e6    # 1900000
double2: .double -4.9e5   # -490000

zero: .float 0.0 

add_res: .skip 120
sub_res: .skip 120
mul_res: .skip 120
div_res: .skip 120
add_double_res: .skip 120
sub_double_res: .skip 120
mul_double_res: .skip 120
div_double_res: .skip 120
nan_res: .skip 120
positive_res: .skip 120
negative_res: .skip 120
zero_ne_res: .skip 120
zero_pos_res: .skip 120



double: .long 0x27f
float: .long 0x7f

round_zero: .long 0xf7f
round_down: .long 0x77f
round_up: .long 0xb7f 

.text

.global _start

_start:

  fninit
  fwait

  call nan
  call infinity_positive
  call infinity_negative
  call zero_positive 
  call zero_negative
  call add  
  call sub
  call mul
  call div 

  call add_double 
  call sub_double 
  call mul_double 
  call div_double 

  fldcw round_zero #zaokraglij do zera
  call div_double
  fldcw round_down #w dół
  call div_double
  fldcw round_up #do góry
  call div_double

  end:
  mov $SYSEXIT, %eax
  mov $EXIT_SUCCESS, %ebx
  int $0x80

nan:
  fld zero 
  fld zero 
  fdivp 
  fstp nan_res
  ret 

infinity_positive:
  fld zero 
  fld float1 
  fdivp 
  fstp positive_res
  ret 

infinity_negative:
  fld zero 
  fld float3
  fdivp 
  fstp negative_res
  ret 

zero_positive:
  fld float1
  fld zero 
  fdivp
  fstp zero_pos_res 
  ret 

zero_negative:
  fld float3 
  fld zero 
  fdivp
  fstp zero_ne_res 
  ret 

add:
  fld float1 
  fld float2 
  faddp # dodanie 
  
  fstp add_res
  ret 

sub:
  fld float1 
  fld float2 
  fsubp # odjęcie 
  fstp sub_res
  ret 

div:
  fld float1 
  fld float2 
  fdivp # podzielenie 
  fstp div_res 
  ret 

mul:
  fld float1 
  fld float2 
  fmulp # pomnozenie 
  fstp mul_res 
  ret 
add_double:
  fldl double1 
  fldl double2 
  faddp # dodanie 
  
  fstp add_double_res
  ret 

sub_double:
  fldl double1 
  fldl double2 
  fsubp # odjęcie 
  
  fstp sub_double_res
  ret 

div_double:
  fldl double1 
  fldl double2 
  fdivp # podzielenie 
  
  fstp div_double_res 
  ret 

mul_double:
  fldl double1 
  fldl double2 
  fmulp # pomnożenie 
  
  fstp mul_double_res 
  ret 


  
