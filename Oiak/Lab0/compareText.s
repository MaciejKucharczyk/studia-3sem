SYSOPEN = 5
SYSWRITE = 4
SYSREAD = 3
STDIN = 0
STDOUT = 1
EXIT_SUCCESS = 0
SYSEXIT = 1

.section .data
buf: .ascii "     "
buf_len = . - buf

var: .ascii "abcde"
var_len = . - var

.section .text
msg: .ascii "Write text (5): \n"
msg_len = . - msg

msg2: .ascii "Incorrect"
msg2_len = . - msg2

msg3: .ascii "Correct"
msg3_len = . - msg3

newline: .ascii "\n"
newline_len = . - newline

.global _start
_start:

# wypisanie komendy 
mov $SYSWRITE, %eax
mov $STDOUT, %ebx
mov $msg, %ecx
mov $msg_len, %edx
int $0x80

# wprowadzenie tesktu
movl $STDIN, %ebx
movl $SYSREAD, %eax
movl $buf, %ecx
movl $buf_len, %edx
int $0x80

# porownanie wpisanego tekstu do tekstu zapisanego w val
movl buf, %eax
movl var, %ebx
cmp %eax, %ebx
JNE not_equal
JMP equal

equal:
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $msg3, %ecx
	mov $msg3_len, %edx
	int $0x80
	JMP end
	
not_equal:

	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $msg2, %ecx
	mov $msg2_len, %edx
	int $0x80
	
end:
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $newline, %ecx
	mov $newline_len, %edx
	int $0x80

	mov $SYSEXIT, %eax
	mov $EXIT_SUCCESS, %ebx
	int $0x80
